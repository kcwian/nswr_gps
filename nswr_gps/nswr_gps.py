import nav_msgs
import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from std_msgs.msg import Header
from sensor_msgs.msg import NavSatFix, NavSatStatus
import utm
import math

import math

class GPS(Node):

    def __init__(self):
        super().__init__('nswr_gps')

        #  GPS Fix subscribers
        # self.subscription_tapas = self.create_subscription()
        
        # Create topic for odometry publishers
        # self.publisher_tapas_odom = self.create_publisher()

        self.easting_offset = 0
        self.northing_offset = 0
        self.alt_offset = 0
        
        # Initalize empty file
        with open("tapas.txt", 'w') as file:
            file.close()

    def tapas_callback(self, fix):

        if fix.status.status == NavSatStatus.STATUS_NO_FIX:
            return

        # Convert latitude and longitude to UTM coordinates
        # u = utm.from_latlon()

        # Declare odometry message
        odom = Odometry()

        # Fill odometry message time and frames
        odom.header.stamp = fix.header.stamp
        odom.header.frame_id = "map"
        odom.child_frame_id = fix.header.frame_id

        #  Fill "position" in odom message (https://docs.ros2.org/foxy/api/nav_msgs/msg/Odometry.html)
        #  easting: the x coordinate
        #  northing: the y coordinate
        #  fix.altitude: the z coordinate





        # Publish odometry message
        self.publisher_tapas_odom.publish(odom)


def main(args=None):

    rclpy.init(args=args)
    gps = GPS()

    rclpy.spin(gps)

    gps.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()